<?php

/**
 * @file
 * This file contains the Workflow-ng hooks and functions necessary to make the
 * order related entity, conditions, events, and actions work.
 */


/******************************************************************************
 * Workflow-ng Hooks                                                          *
 ******************************************************************************/

// Tell Workflow about the various payment events.
function uc_eway_event_info(){
  $events['eway_payment_processed'] = array(
    '#label' => t('A payment gets successfully processed for an order'),
    '#module' => t('eWay'),
    '#arguments' => array(
      'order' => array('#entity' => 'order', '#label' => t('Order')),
    ),
  );

  return $events;
}

/**
 * Implementation of hook_configuration().
 *
 * Connect the auspost action and event.
 */
function uc_eway_configuration(){
	// Create a workflow_ng configuration
	$configurations = array(
		'uc_eway_payment_processed' => array(
      	'#label' => t('Successful payment via eWay'),
      	'#event' => 'eway_payment_processed',
      	'#module' => 'uc_eway',
      	'#active' => variable_get('change_order_status', 'true'),
    	),
	);
  
	//TODO: uc_eway: Get the workflow condition to work.
	// Configure a condition for the workflow. If order balance is less than or equal to 0
//	$condition = workflow_ng_use_condition('uc_payment_condition_balance', array (
//		'#type' => 'condition',
//		'#name' => 'uc_payment_condition_balance',
//		'#argument map' => array ('order' => 'order'),
//		'#settings' => array (
//			'balance_comparison' => 'less_equal',
//      	),
//    ));

    
	// Configure an action to be used by this configuration
	$action = workflow_ng_use_action('uc_order_action_update_status', array(
		'#type' => 'action',
  		'#name' => 'uc_order_action_update_status',
  		'#argument map' => array('order' => 'order'),
  		'#settings' => array('order_status' => 'payment_received')
	));
  
	$configurations['uc_eway_payment_processed'] = workflow_ng_configure($configurations['uc_eway_payment_processed'], $action);
  
  return $configurations;
}